import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Static Title';
  api = environment.api;
  constructor(private http: HttpClient) {
    this.http.get<string>(`${this.api} /api/service_2`).subscribe(res => {
      this.title = res;
    });
  }
}
